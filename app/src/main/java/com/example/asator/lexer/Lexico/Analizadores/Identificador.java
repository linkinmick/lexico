package com.example.asator.lexer.Lexico.Analizadores;

import com.example.asator.lexer.Lexico.Lexico;
import com.example.asator.lexer.Lexico.PalabrasReservadas;
import com.example.asator.lexer.Lexico.SimbolosAceptacion;
import com.example.asator.lexer.Lexico.TipoSimbolo;
import com.example.asator.lexer.Lexico.Tipos;

/**
 * Created by mfrausto on 8/24/16
 */
public class Identificador implements AnalizadorToken {

    private Lexico lexico;

    public Identificador(Lexico lexico){
        this.lexico = lexico;
    }

    @Override
    public void sigueAnalizando() {
        lexico.simbolo += lexico.getC();
        lexico.setC(lexico.sigCaracter());

        if (lexico.esLetra(lexico.getC()) || lexico.esDigito(lexico.getC())) {
            sigueAnalizando();
        } else if (SimbolosAceptacion.esSimboloAceptacion(lexico.getC())
                || lexico.getC().equals('"')
                || lexico.getC().equals('<')
                || lexico.getC().equals('>')
                || lexico.getC().equals('=')
                || lexico.getC().equals('!')
                || lexico.getC().equals('|')
                || lexico.getC().equals('&')
                ) {
            //Antes de aceptacion verifica si es palabra reservada o tipo
            if(PalabrasReservadas.esReservada(lexico.simbolo)) {
                lexico.aceptacion(PalabrasReservadas.mapaPalabras.get(lexico.simbolo));
            }else if (Tipos.esTipo(lexico.simbolo)){
                lexico.aceptacion(Tipos.mapaTipos.get(lexico.simbolo));
            }else{
                lexico.aceptacion(TipoSimbolo.IDENTIFICADOR);
            }
            lexico.customRetroceso(true);
        } else {
            //Antes de aceptacion verifica si es palabra reservada o tipo
            if(PalabrasReservadas.esReservada(lexico.simbolo)){
                lexico.aceptacion(PalabrasReservadas.mapaPalabras.get(lexico.simbolo));
            }else if (Tipos.esTipo(lexico.simbolo)){
                lexico.aceptacion(Tipos.mapaTipos.get(lexico.simbolo));
            }else{
                lexico.aceptacion(TipoSimbolo.IDENTIFICADOR);
            }
            lexico.customRetroceso(false);
        }
    }
}