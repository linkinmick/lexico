package com.example.asator.lexer.Lexico;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mfrausto on 8/25/16
 */
public class PalabrasReservadas {

    public static Map<String,Integer> mapaPalabras = new HashMap<>(); //Mapa de palabras reservadas
    static{
        /*
            Inicializa mapa de palabras reservadas
            De la forma mapaSimbolos.put(PALABRA, ESTADO_DE_ACEPTACION)
            Donde llave es la Palabra y valor el estado de aceptacion al que llevan
        */
        mapaPalabras.put("if", TipoSimbolo.IF);
        mapaPalabras.put("while", TipoSimbolo.WHILE);
        mapaPalabras.put("return", TipoSimbolo.RETURN);
        mapaPalabras.put("else", TipoSimbolo.ELSE);

    }

    public static Boolean esReservada(String cadena){
        return mapaPalabras.containsKey(cadena);
    }

}