package com.example.asator.lexer.Lexico;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mfrausto on 8/25/16
 */
public class SimbolosAceptacion {

    public static Map<Character, Integer> mapaSimbolos = new HashMap<>(); //SIMBOLOS QUE LLEVAN A ESTADO DE ACEPTACION
    static{
        /*
            Inicializa mapa de Simbolos que llevan a aceptacion
            De la forma mapaSimbolos.put(LLAVE, VALOR)
            Donde llave es Simbolo y valor el estado de aceptacion al que llevan
        */

        mapaSimbolos.put('+', TipoSimbolo.OPADIC); //OPERADOR ADICION
        mapaSimbolos.put('-', TipoSimbolo.OPADIC); //OPERADOR ADICION
        mapaSimbolos.put('*', TipoSimbolo.OPMULT); //OPERADOR MULTIPLICACION
        mapaSimbolos.put('/', TipoSimbolo.OPMULT); //OPERADOR MULTIPLICACION
        mapaSimbolos.put(';', TipoSimbolo.PUNTOCOMA); //PUNTO Y COMA
        mapaSimbolos.put(',', TipoSimbolo.COMA); //COMA
        mapaSimbolos.put('(', TipoSimbolo.PARAPE); //PARENTESIS APERTURA
        mapaSimbolos.put(')', TipoSimbolo.PARCER); //PARENTESIS CIERRE
        mapaSimbolos.put('{', TipoSimbolo.LLAVEAPE); //LLAVE APERTURA
        mapaSimbolos.put('}', TipoSimbolo.LLAVECER); //LLAVE CIERRE

    }

    public static Boolean esSimboloAceptacion(Character car){
        return mapaSimbolos.containsKey(car);
    }

}