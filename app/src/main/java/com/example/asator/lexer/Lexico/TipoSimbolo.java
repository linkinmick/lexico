package com.example.asator.lexer.Lexico;

/**
 * Created by mfrausto on 8/21/16
 */
public class TipoSimbolo {
    public static final int ERROR = -1;
    public static final int IDENTIFICADOR = 0;
    public static final int ENTERO = 1;
    public static final int REAL = 2;
    public static final int CADENA = 3;
    public static final int TIPO = 4;
    public static final int OPADIC = 5;
    public static final int OPMULT = 6;
    public static final int OPREL = 7;
    public static final int OPOR = 8;
    public static final int OPAND = 9;
    public static final int OPNOT = 10;
    public static final int OPIGUALDAD = 11;
    public static final int PUNTOCOMA = 12;
    public static final int COMA = 13;
    public static final int PARAPE = 14;
    public static final int PARCER = 15;
    public static final int LLAVEAPE = 16;
    public static final int LLAVECER = 17;
    public static final int IGUAL = 18;
    public static final int IF = 19;
    public static final int WHILE = 20;
    public static final int RETURN = 21;
    public static final int ELSE = 22;
    public static final int PESOS = 23;


}