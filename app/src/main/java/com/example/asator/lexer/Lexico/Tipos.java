package com.example.asator.lexer.Lexico;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mfrausto on 8/25/16
 */
public class Tipos {

    public static Map<String,Integer> mapaTipos = new HashMap<>(); //Mapa de tipos
    static{
        /*
            Inicializa mapa de palabras reservadas
            De la forma mapaSimbolos.put(PALABRA, ESTADO_DE_ACEPTACION)
            Donde llave es la Palabra y valor el estado de aceptacion al que llevan
        */
        mapaTipos.put("int", TipoSimbolo.TIPO);
        mapaTipos.put("float", TipoSimbolo.TIPO);
        mapaTipos.put("void", TipoSimbolo.TIPO);
        mapaTipos.put("double", TipoSimbolo.TIPO);
    }

    public static Boolean esTipo(String cadena){
        return mapaTipos.containsKey(cadena);
    }
}
