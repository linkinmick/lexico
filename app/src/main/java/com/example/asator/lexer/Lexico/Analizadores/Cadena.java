package com.example.asator.lexer.Lexico.Analizadores;


import com.example.asator.lexer.Lexico.Lexico;
import com.example.asator.lexer.Lexico.TipoSimbolo;

/**
 * Created by mfrausto on 8/24/16
 */
public class Cadena extends Lexico implements AnalizadorToken {

    private Lexico lexico;

    public Cadena(Lexico lexico){
        this.lexico = lexico;
    }

    @Override
    public void sigueAnalizando() {
        lexico.simbolo += lexico.getC();
        lexico.setC(lexico.sigCaracter());

        if(!lexico.getC().equals('"')){
            this.sigueAnalizando();
        }else{
            lexico.aceptacion(TipoSimbolo.CADENA);
        }
    }

}
