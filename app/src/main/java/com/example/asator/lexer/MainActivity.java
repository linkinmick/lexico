package com.example.asator.lexer;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.asator.lexer.Adaptadores.TokensAdapter;
import com.example.asator.lexer.Lexico.Lexico;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private List tokens;

    private EditText edtEntrada;
    private Button btnAnalizar;
    private ListView listaTokens;

    private TokensAdapter adapter;
    private Lexico lexico;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtEntrada = (EditText) findViewById(R.id.edtEntrada);
        btnAnalizar = (Button) findViewById(R.id.btnAnalizar);
        listaTokens = (ListView) findViewById(R.id.listaTokens);

        lexico = new Lexico();
    }

    public void analizaEntrada(View view) {

        String entrada = edtEntrada.getText().toString();
        tokens = new ArrayList();

        lexico.entrada(entrada);
        /*lexico.entrada(
                "+\"h\"+Identi- +Identi2- 3 = 3 pui >= pui2 > pui3 < pui4 <= pui5.32 44.4 34. 33 s || && ! != == = "+
                        "a = b pui >= pui2 > pui3 < pui4 <= pui5"+
                        " >= pui2 > pui3 < pui4 <= pui5"+
                        "aifb if If While while whiles elses else rreturn Areturn return"
        );*/

        //Log.e("COMPILADOR", "Resultado del Análisis Lexico");
        //Log.e("COMPILADOR", "Simbolo\t\tTipo");

        while (lexico.simbolo.compareToIgnoreCase("$") != 0){
            List<String> valores = new ArrayList<>();

            Integer tipo = lexico.sigSimbolo();
            valores.add(lexico.simbolo);
            valores.add(lexico.tipoAcad(lexico.tipo));
            tokens.add(valores);
            //Log.e("COMPILADOR", lexico.simbolo + "\t\t" + lexico.tipoAcad(lexico.tipo));
        }

        adapter = new TokensAdapter(this, tokens);
        listaTokens.setAdapter(adapter);
    }

}
