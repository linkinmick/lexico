package com.example.asator.lexer.Lexico;

/**
 * Created by mfrausto on 8/21/16
 */
public abstract class AbstractLexico {

    private String fuente;
    private Integer ind;
    private Boolean continua;
    private Character c;
    private Integer estado;

    public String simbolo = "";
    public Integer tipo;

    public abstract Character sigCaracter();
    public abstract void sigEstado(Integer estado);
    public abstract void aceptacion(Integer estado);
    public abstract Boolean esLetra(Character c);
    public abstract Boolean esDigito(Character c);
    public abstract Boolean esEspacio(Character c);
    public abstract void retroceso();

    public abstract void entrada(String fuente);
    public abstract String tipoAcad(Integer tipo);
    public abstract Integer sigSimbolo();
    public abstract Boolean terminado();


    public String getFuente() {
        return fuente;
    }

    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    public Integer getInd() {
        return ind;
    }

    public void setInd(Integer ind) {
        this.ind = ind;
    }

    public Boolean getContinua() {
        return continua;
    }

    public void setContinua(Boolean continua) {
        this.continua = continua;
    }

    public Character getC() {
        return c;
    }

    public void setC(Character c) {
        this.c = c;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }
}
