package com.example.asator.lexer.Lexico.Analizadores;

import com.example.asator.lexer.Lexico.Lexico;
import com.example.asator.lexer.Lexico.SimbolosAceptacion;
import com.example.asator.lexer.Lexico.TipoSimbolo;

/**
 * Created by mfrausto on 8/25/16
 */
public class Real extends Lexico implements AnalizadorToken  {

    private Lexico lexico;

    public Real(Lexico lexico){
        this.lexico = lexico;
    }

    @Override
    public void sigueAnalizando() {
        lexico.simbolo += lexico.getC();
        lexico.setC(lexico.sigCaracter());

        if(lexico.esDigito(lexico.getC())){
            sigueAnalizando();
        } else if (SimbolosAceptacion.esSimboloAceptacion(lexico.getC())
                || lexico.getC().equals('"')
                || lexico.getC().equals('<')
                || lexico.getC().equals('>')
                || lexico.getC().equals('=')
                || lexico.getC().equals('!')
                || lexico.getC().equals('|')
                || lexico.getC().equals('&')
                || lexico.esLetra(lexico.getC())
                ) {
            lexico.aceptacion(TipoSimbolo.REAL);
            lexico.customRetroceso(true);
        } else {
            lexico.aceptacion(TipoSimbolo.REAL);
            lexico.customRetroceso(false);
        }

    }
}