package com.example.asator.lexer.Lexico;


import com.example.asator.lexer.Lexico.Analizadores.Cadena;
import com.example.asator.lexer.Lexico.Analizadores.Digito;
import com.example.asator.lexer.Lexico.Analizadores.Identificador;
import com.example.asator.lexer.Lexico.Analizadores.Igualdad;
import com.example.asator.lexer.Lexico.Analizadores.OpAnd;
import com.example.asator.lexer.Lexico.Analizadores.OpNot;
import com.example.asator.lexer.Lexico.Analizadores.OpOr;
import com.example.asator.lexer.Lexico.Analizadores.OpRelacional;
import com.example.asator.lexer.Lexico.Analizadores.Real;
import com.example.asator.lexer.Lexico.Utils.Utils;

/**
 * Created by mfrausto on 8/21/16
 */
public class Lexico extends AbstractLexico {

    protected Cadena cadena;
    protected Identificador identificador;
    protected OpRelacional opRelacional;
    protected Digito digito;
    protected Real real;
    protected Igualdad igualdad;
    protected OpNot opNot;
    protected OpAnd opAnd;
    protected OpOr opOr;

    //CONSTRUCTORES
    public Lexico(String fuente) {
        this.setInd(0);
        this.setFuente(fuente);
    }

    public Lexico() {
        this.setInd(0);
    }

    /*------------------------------------
     IMPLEMENTACION DE METODOS ABSTRACTOS
    -------------------------------------*/
    @Override
    public String tipoAcad(Integer tipo) {
        if(Utils.existeSimbolo(tipo)){
            return Utils.mapaTipos.get(tipo);
        }else {
            return "Tipo no encontrado";
        }
    }

    @Override
    public void entrada(String fuente) {
        this.setInd(0);
        this.setFuente(fuente);

        //TODO: Buscar donde mover estas inicializaciones
        cadena = new Cadena(this);
        identificador = new Identificador(this);
        opRelacional = new OpRelacional(this);
        digito = new Digito(this);
        real = new Real(this);
        igualdad = new Igualdad(this);
        opNot = new OpNot(this);
        opAnd = new OpAnd(this);
        opOr = new OpOr(this);
    }

    @Override
    public Integer sigSimbolo() {
        this.setEstado(555);
        this.setContinua(true);
        this.simbolo = "";

        //Inicio del Automata
        while (getContinua()) {

            this.setC(sigCaracter());

            switch (this.getEstado()) {
                case 555:
                    if(SimbolosAceptacion.esSimboloAceptacion(this.getC())){
                        aceptacion(SimbolosAceptacion.mapaSimbolos.get(this.getC()));
                    } else if (esDigito(this.getC())){ //DIGITOS Y REALES
                        digito.sigueAnalizando();
                    } else if (this.getC().equals('"')) { //CADENAS
                        cadena.sigueAnalizando();
                    } else if (esLetra(this.getC())) { // IDENTIFICADOR
                        identificador.sigueAnalizando();
                    } else if (this.getC().equals('<') || this.getC().equals('>')) { //OPERADOR RELACIONAL
                        opRelacional.sigueAnalizando();
                    } else if (this.getC().equals('=')) { //ASIGNACION Y OPERADOR IGUALDAD
                        igualdad.sigueAnalizando();
                    } else if (this.getC().equals('!')){ //OPERADOR NOT Y OPERADOR IGUALDAD
                        opNot.sigueAnalizando();
                    } else if (this.getC().equals('|')){ //OPERADOR OR
                        opOr.sigueAnalizando();
                    } else if (this.getC().equals('&')){ //OPERADOR AND
                        opAnd.sigueAnalizando();
                    }

                    else if(esEspacio(this.getC())){ //ESPACIO EN BLANCO
                        break;
                    } else if (this.getC().equals('$')){ //FIN DE ENTRADA
                        aceptacion(TipoSimbolo.PESOS);
                    }
                    else { //ERROR
                        aceptacion(TipoSimbolo.ERROR);
                    }

                    break;

            }

        }
        //Fin de Automata

        //Asigna tipo de acuerdo a estado [Antes switch aqui]

        /*if(EstadosAceptacion.existeEstado(this.getEstado())){
            this.tipo = EstadosAceptacion.mapaEstados.get(this.getEstado());
        }else{
            this.tipo = TipoSimbolo.ERROR;
        }*/

        if(this.getEstado() != null){
            this.tipo = this.getEstado();
        }else{
            this.tipo = TipoSimbolo.ERROR;
        }

        return this.tipo;
    }


    @Override
    public Character sigCaracter() {
        if (terminado()) {
            return '$';
        }

        int ind = getInd();
        Character fuente = getFuente().charAt(ind++);
        this.setInd(ind);
        return fuente;
    }

    @Override
    public void sigEstado(Integer estado) {
        this.setEstado(estado);
        this.simbolo += this.getC();
    }

    @Override
    public void aceptacion(Integer estado) {
        sigEstado(estado);
        this.setContinua(false);
    }

    @Override
    public Boolean terminado() {
        return this.getInd() >= getFuente().length();
    }

    @Override
    public Boolean esLetra(Character c) {
        return Character.isAlphabetic(c);
    }

    @Override
    public Boolean esDigito(Character c) {
        return Character.isDigit(c);
    }

    @Override
    public Boolean esEspacio(Character c) {
        return c.equals(' ') || c.equals('\t') || c.equals('\n');
    }

    @Override
    public void retroceso() {
        if (!this.getC().equals('$')) {
            int ind = this.getInd();
            this.setInd(--ind);
            this.setContinua(false);
        }
    }

    /*---------------------------
     METODOS AGREGADOS AL LEXER
    ----------------------------*/

    public void customRetroceso(Boolean conocido) {
        if (this.simbolo != null && this.simbolo.length() > 0) {
            this.simbolo = eliminaUltimoCaracter(this.simbolo);
        }

        if (conocido) {
            int ind = this.getInd();
            this.setInd(--ind);
        }
    }

    private static String eliminaUltimoCaracter(String str) {
        return str.substring(0, str.length() - 1);
    }

    /*------------------
     GETTERS Y SETTERS
    ------------------*/
    public Real getReal() {
        return real;
    }
}
