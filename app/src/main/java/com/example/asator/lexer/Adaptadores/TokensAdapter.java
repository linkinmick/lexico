package com.example.asator.lexer.Adaptadores;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.asator.lexer.Lexico.Utils.Utils;
import com.example.asator.lexer.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mfrausto on 8/25/16
 */
public class TokensAdapter extends ArrayAdapter {

    private Context contexto;
    private List<ArrayList<String>> tokens;

    public TokensAdapter(Context contexto, List<ArrayList<String>> tokens){
        super(contexto, R.layout.lista_tokens, tokens);
        this.contexto = contexto;
        this.tokens = tokens;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.lista_tokens, parent, false);

        TextView lblSimbolo = (TextView) rowView.findViewById(R.id.lblSimbolo);
        TextView lblTipo = (TextView) rowView.findViewById(R.id.lblTipo);

        lblSimbolo.setText(tokens.get(position).get(0));
        lblTipo.setText(tokens.get(position).get(1));

        return rowView;
    }
}
