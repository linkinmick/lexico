package com.example.asator.lexer.Lexico.Utils;

import com.example.asator.lexer.Lexico.TipoSimbolo;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by mfrausto on 8/25/16
 */
public class Utils {

    public static Map<Integer, String> mapaTipos = new HashMap<>();
    static{
        mapaTipos.put(TipoSimbolo.ERROR, "Error");
        mapaTipos.put(TipoSimbolo.IDENTIFICADOR, "Identificador");
        mapaTipos.put(TipoSimbolo.OPADIC, "Op.Adicion");
        mapaTipos.put(TipoSimbolo.OPMULT, "Op. Multiplicacion");
        mapaTipos.put(TipoSimbolo.PESOS, "Fin de la Entrada");
        mapaTipos.put(TipoSimbolo.ENTERO, "Entero");
        mapaTipos.put(TipoSimbolo.REAL, "Real");
        mapaTipos.put(TipoSimbolo.CADENA, "Cadena");
        mapaTipos.put(TipoSimbolo.PUNTOCOMA, "Punto y coma");
        mapaTipos.put(TipoSimbolo.COMA, "Coma");
        mapaTipos.put(TipoSimbolo.PARAPE, "Parentesis");
        mapaTipos.put(TipoSimbolo.PARCER, "Parentesis");
        mapaTipos.put(TipoSimbolo.LLAVEAPE, "Llave");
        mapaTipos.put(TipoSimbolo.LLAVECER, "Llave");
        mapaTipos.put(TipoSimbolo.IGUAL, "Asignacion");
        mapaTipos.put(TipoSimbolo.OPREL, "Op.Relacional");
        mapaTipos.put(TipoSimbolo.OPIGUALDAD, "Op.Igualdad");
        mapaTipos.put(TipoSimbolo.OPNOT, "Op.Not");
        mapaTipos.put(TipoSimbolo.OPOR, "Op.Or");
        mapaTipos.put(TipoSimbolo.OPAND, "Op.And");
        mapaTipos.put(TipoSimbolo.IF, "Palabra reservada");
        mapaTipos.put(TipoSimbolo.ELSE, "Palabra reservada");
        mapaTipos.put(TipoSimbolo.RETURN, "Palabra reservada");
        mapaTipos.put(TipoSimbolo.WHILE, "Palabra reservada");
        mapaTipos.put(TipoSimbolo.TIPO, "Tipo");

    }

    public static Boolean existeSimbolo(Integer simbolo){
        return mapaTipos.containsKey(simbolo);
    }
}
